
// import {createRequire} from 'module';
// const require = createRequire(import.meta.url);


import createError from 'http-errors';
import cookieParser  from 'cookie-parser';
import logger  from 'morgan';
import { express } from './legacy-export.js';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

// const cookieParser = require('cookie-parser');


import { router as indexRouter } from './routes/index.js';
import { router as mongoRouter } from './routes/mongo.js'; 
import { router as usersRouter } from './routes/users.js'; 

const app = express();

// view engine setup
// m app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static( `${__dirname}/public`));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/mongo', mongoRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export { app };
