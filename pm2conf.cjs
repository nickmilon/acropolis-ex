/* eslint-disable comma-dangle */
/* eslint-disable quote-props */
/* eslint-disable quotes */
module.exports = {
  apps: [
    {
      name: 'nm app',
      script: './bin/www.js',
      node_args: '--experimental-modules ',
      "env": {
        "NODE_ENV": "dev",
        "connWork": "mongodb://localhost:27517/test"
      }
    },
  ],
};
