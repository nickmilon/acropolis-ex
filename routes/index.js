
// import express from 'express';
import listEndpoints from 'express-list-endpoints';
import { Router } from '../legacy-export.js';

// import  { application, connWork, serverOpts }  from '../config

const router = Router();

/* GET home page. */
router.get('/tst1', (req, res, next) => {
  res.render('index', { title: 'Express' });
});


router.get('/', (req, res) => {
  const endPoints = listEndpoints(req.app);
  // console.log(endPoints);
  res.render('endpoints', { endPoints });
});

export { router };
