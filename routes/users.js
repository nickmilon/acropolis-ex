
import { Router } from '../legacy-export.js';
import { mgClient } from '../modules/mongoConSingleton.js';

// const { mgClient } = require('../modules/mongoConSingleton');

let glClient = null;
mgClient.connect()
  .then((client) => {
    glClient = client;
  });

const router = Router();


/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

router.get('/mongo', async (req, res) => {
  // const testColl = glClient.getCollection('db_test', 'col_test');
  const testColl = req.app.locals.mgClient.getCollection('db_test', 'col_test');
  // const testColl = req.mgConn.getCollection('db_test', 'col_test');
  // const dbRes = await testColl.findOneAndUpdate({ foo: 'bar' }, { $set: { foo2: 'bar2' } }, { upsert: true, returnOriginal: true });
  const dbRes = await testColl.find().toArray();
  res.json(dbRes);
});

export { router };
