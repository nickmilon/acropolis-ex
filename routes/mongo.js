import { insertDates } from 'acropolis-mg/mg/benchmarks/analytics.js';
import { Router } from '../legacy-export.js';
import { mgClient } from '../modules/mongoConSingleton.js';

let glClient = null;
mgClient.connect()
  .then((client) => {
    glClient = client;
  });

const router = Router();


/* GET users listing. */
router.get('/', (req, res) => {
  res.send('respond with a resource');
});

router.get('/testGlobalClient', async (req, res) => {
  const testColl = glClient.getCollection('db_test', 'col_test');
  const dbRes = await testColl.find().toArray();
  res.json(dbRes);
});

router.get('/testInjectedClient', async (req, res) => {
  const testColl = req.app.locals.mgClient.getCollection('db_test', 'col_test');
  const dbRes = await testColl.find().toArray();
  res.json(dbRes);
});

router.get('/insertDates/:name/', async (req, res) => {
  const { name } = req.params; // req.query;
  // console.log(req.query);
  // const insertDates = async (coll, { name = '01', maxCnt = 10000, reportEvery = 1000, w = 0, logger } = {}) => {
  const testColl = req.app.locals.mgClient.getCollection('db_test', 'col_dates');
  const dbRes = insertDates(testColl, { name, maxCnt: 100 * 1000000, reportEvery: 500000, logger: console });
  res.json(dbRes);
});

export {
  router,
};
