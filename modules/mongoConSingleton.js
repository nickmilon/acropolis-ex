
// import {createRequire} from 'module';
// const require = createRequire(import.meta.url);

// const { MgClientPlus } = require('acropolis-mg').plusClient;

import { MgClientPlus } from  'acropolis-mg/mg/plus/client.js';
import { connWork } from '../config.js';
import { logger } from './logger.js';

connWork.logger = logger;
const mgClient = new MgClientPlus(connWork);
export { mgClient };
