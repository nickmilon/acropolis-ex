/* eslint-disable no-underscore-dangle */
/** express extentions
 *
 */

/**
 * works only for simple endpoints (no router) defined as  app.get('/admin/dbstats' ...)
 * for more complicated stuff see   require('express-list-endpoints')
 * @param {object} appExp an express app object
 */
const expressEndPoints = (appExp) => {
  const getMethods = methods => Object.keys(methods).map(k => k);
  const routes = appExp._router.stack;
  return routes.filter(r => r.route)
    .map(r => [r.route.path, getMethods(r.route.methods)]);
};

module.exports = {
  expressEndPoints,
};
