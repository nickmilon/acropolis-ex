// import { createRequire } from 'module';
// const require = createRequire(import.meta.url);

import winston from 'winston';

// const { createLogger, format, transports } = require('winston');
const { createLogger, format, transports } = winston;

const { combine, timestamp, json, colorize, printf, splat, label } = format;

// const tsFormat = () => (new Date()).toISOString();
// const myFormat = printf(info => (`${info.timestamp}${info.label} ${info.level}: ${JSON.stringify(info.message)}`));
const loggerJson = createLogger({
  format: combine(
    colorize(),
    timestamp({ format: 'MM-DD-HH:mm:ss' }),
    json(),
  ),
  transports: [new transports.Console({ level: (process.env.NODE_ENV === 'production') ? 'info': 'debug' })],
});

const logger = createLogger({
  format: combine(
    colorize(),
    label({ label: '|' }),
    timestamp({ format: 'MM-DD-HH:mm:ss' }),
    // splat(),
    printf(info => (`${info.timestamp}${info.label}${info.level}: ${JSON.stringify(info.message)}`)),
  ),
  transports: [new transports.Console()],
});


export {
  logger,
  loggerJson,
};
