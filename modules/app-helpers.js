import { DummyLogger } from 'acropolis-nd/hellas/rodos.js';
import { sleepMs, objectToStrLevel1 } from 'acropolis-nd/hellas/delphi.js';
/**
 * encapsulates http server, app and mongo Client
 */
class AppPlus {
  constructor(app, server, mgClient, { logger = DummyLogger, serverOpts = {} } = {}) {
    this._props = { app, server, mgClient, logger, serverOpts };
    this._started = false;
  }

  get app() { return this._props.app; }

  get server() { return this._props.server; }

  get mgClient() { return this._props.mgClient; }

  get logger() { return this._props.logger; }

  get serverOpts() { return this._props.serverOpts; }

  serverConnectionsCount() { return this.server.getConnections((error, count) => count);}

  serverAddressStr() { return objectToStrLevel1(this.server.address()); }

  serverOnListening() { this.logger.info(`server listening on: ${this.serverAddressStr()}`); }

  serverOnError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    } else {
      switch (error.code) {
        case 'EACCES':
          this.logger.error(`${objectToStrLevel1(this.serverOpts)} requires elevated privileges`);
          process.exit(1);
          break;
        case 'EADDRINUSE':
          this.logger.error(`${objectToStrLevel1(this.serverOpts)} is already in use`);
          process.exit(1);
          break;
        default:
          throw error;
      }
    }
  }

  async appStart() {
    try {
      await this.mgClient.connect();
      this.app.locals.mgClient = this.mgClient;
      this.server.listen(this.serverOpts.port, this.serverOpts.host || 'localhost');
      this.server.on('error', async err => this.serverOnError(err));
      this.server.on('listening', async () => this.serverOnListening());
      this.server.on('close', () => this.logger.info(`server: ${objectToStrLevel1(this.serverOpts)} is closing`));

      process.on('SIGINT', async () => { this.appStop('received SIGINT'); });
      process.on('SIGTERM', async () => { this.appStop('received SIGTERM'); });
      process.on('uncaughtException', (err) => { if (err) this.logger.error(`Uncaught Exception: ${err.message}`, err.stack); });
      process.addListener('SIGPIPE', (err) => { if (err) this.logger.error('SIGPIPE', err.stack); });
      this._started = true;
    } catch (err) {
      this.logger.error(err);
      process.exit(1);
    }
  }

  async appStop(reason = 'turned down') {
    if (this._started === true) {
      try {
        this.logger.error(`server stopping: reason:${reason}`);
        this.server.close();
        await this.mgClient.destroy(reason);
        await sleepMs(300);
        this._started = false;
        process.exit(0);
      } catch (err) {
        this.logger.error(err);
        process.exit(1);
      }
    }
  }
}

export {
  AppPlus,
};
