/**
 * remaps some legacy packaged to ESM import  so we don't re-import all the time
 * @todo: revisit when things get stable
 * for other libraries use;
 *    import { createRequire } from 'module';
 *    const require = createRequire(import.meta.url);
 * console.log({ express: Object.keys(express).sort()})
 */

import express from 'express';

export const {
  Route,
  Router,
  application,
  json,
  query,
  request,
  response,
  urlencoded,
} = express;
export { express };
