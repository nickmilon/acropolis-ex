#!/usr/bin/env node

/**
 * notes:
 *  - server options etc: https://nodejs.org/api/http.html#http_server_listen
 *
 */

import * as http from 'http';
import { sleepMs, objectToStrLevel1 } from 'acropolis-nd/hellas/delphi.js';
import { app } from '../app.js';
import { serverOpts } from '../config.js';
import { logger } from '../modules/logger.js';
import { mgClient } from '../modules/mongoConSingleton.js';

const server = http.createServer(app);

const serverAddressStr = () => objectToStrLevel1(server.address());

const serverOnListening = () => logger.info(`server listening on: ${serverAddressStr()}`);

const serverOnError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  } else {
    switch (error.code) {
      case 'EACCES':
        logger.error(`${objectToStrLevel1(serverOpts)} requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        logger.error(`${objectToStrLevel1(serverOpts)} is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
};

// eslint-disable-next-line no-undef
/*
const connectToMongoAsync = async () => {
  connWork.logger = logger;
  mgClient = new MgClientPlus(connWork);
  try {
    await mgClient.connect();
    const conn = await Connection.connect;
    console.log({ conn })
    const testColl = mgClient.getCollection('db_test', 'col_test');
    await testColl.findOneAndUpdate({ foo: 'bar' }, { $set: { foo2: 'bar2' } }, { upsert: true, returnOriginal: true }); // test connection
    app.locals.mgClient = mgClient;
    return mgClient;
  } catch (err) {
    logger.error({ connectToMongo: 'err', err });
    return err;
  }
};
*/

const appStart = async () => {
  try {
    await mgClient.connect();
    app.locals.mgClient = mgClient;
    server.listen(serverOpts.port, serverOpts.host || 'localhost');
    server.on('error', serverOnError);
    server.on('listening', serverOnListening);
    server.on('close', () => logger.info(`server: ${objectToStrLevel1(serverOpts)} is closing`));
    // await mgClient.connect();
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
};

const appStop = async (reason = 'turned down') => {
  try {
    logger.error(`server stopping: reason:${reason}`);
    server.close();
    await mgClient.destroy(reason);
    await sleepMs(300);
    process.exit(0);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
};

const serverConnectionsCount = () => {
  server.getConnections((error, count) => count);
};

process.on('SIGINT', async () => { appStop('received SIGINT'); });
process.on('SIGTERM', async () => { appStop('received SIGTERM'); });
process.on('uncaughtException', (err) => { if (err) logger.error(`Uncaught Exception: ${err.message}`, err.stack); });
process.addListener('SIGPIPE', (err) => { if (err) logger.error('SIGPIPE', err.stack); });
appStart();
export {
  server,
  serverConnectionsCount,
  mgClient,
};
