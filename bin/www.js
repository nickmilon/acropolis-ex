#!/usr/bin/env node

/**
 * notes:
 *  - server options etc: https://nodejs.org/api/http.html#http_server_listen
 *
 */

import * as http from 'http';

import { sleepMs, objectToStrLevel1 } from 'acropolis-nd/hellas/delphi.js';
import { app } from '../app.js';
import { serverOpts } from '../config.js';
import { logger } from '../modules/logger.js';
import { mgClient } from '../modules/mongoConSingleton.js';
import { AppPlus } from '../modules/app-helpers.js';

const server = http.createServer(app);

const appPlusInst = new AppPlus(app, server, mgClient, { logger, serverOpts });
// constructor(app, server, mgClient, { logger = new DummyLogger(), serverOpts = {} } = {}) {

 
appPlusInst.appStart();
export {
  server,
  mgClient,
  appPlusInst,
};
