/**
 *  configuration module
 *  dbConn1: [arbitrary_connection_name, mongo uri string, options]
 *  if  mongo uri is null will get foobar value from env variable arbitrary_connection_name
 *  i.e.:  export dbWork="mongodb://localhost:27617"
 *  @todo: get from package when json imports are available const { name, version } = require('./package.json');
 */

const application = { name: 'nickmilon_ppp', version: '0.0.1' };
const connWork = { name: 'connWork', connOptions: { forceServerObjectId: true, poolSize: 101, useNewUrlParser: true } };
const serverOpts = { host: '127.0.0.1', port: 3002 };

export { application, connWork, serverOpts };
